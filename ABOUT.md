﻿# About

## На русском

### Упс

Я использую OpenSuse, в которой Qyoto является стандартным пакетом. Проблем нет.

Для Windows готовые бинарные сборки можно загрузить [здесь](https://techbase.kde.org/Development/Languages/Qyoto).
Эти сборки собраны с помощью MinGW, посему <code>mingwm10.dll</code> и <code>libgcc_s_dw2-1.dll</code> должны присутствовать в пути,
как и собственно библиотеки [Qt](http://qt-project.org/downloads).

Жёсткой привязки к версии Qt я не заметил. Может в некоторых местах и есть.) 
В конце концов - исходники открыты и их можно собрать под себя.

Работы ведутся в двух ветках. Ветка по умолчанию под Windows и ветка linux соответственно в Linux. Когда и там и там всё работает - ветки сливаются.

### О примерах

В этот проект вошли примеры из:

* [C# Qyoto tutorial - ZetCode](http://zetcode.com/gui/csharpqyoto/)

### О прочих ресурсах

В этом проекте, в примерах, используются картиночки. Так вот, это картиночки с клёвой коллекции Modern UI Icons.
Проект картиночек открыт, лежит на [GitHub](https://github.com/Templarian/WindowsIcons) и распространяется под лицензией Creative Commons 3.0.

# License

My contribution to the project is licensed under the [Public Domain](https://creativecommons.org/licenses/publicdomain/).

Special cases of licensing see are in files.
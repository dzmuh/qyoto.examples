﻿namespace ScreenShot
{
    using System;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using QtCore;

    using QtGui;

    public class ScreenShot : QWidget
    {
        #region Слоты

        private const string NewScreenshot = "slot_newScreenshot()";

        private const string SaveScreenshot = "slot_saveScreenshot()";

        private const string ShootScreen = "slot_shootScreen()";

        private const string SlotUpdateCheckBox = "slot_updateCheckBox()";
        
        #endregion

        #region Эллементы

        private QPixmap originalPixmap;

        private QLabel screenshotLabel;

        private QGroupBox optionsGroupBox;

        private QSpinBox delaySpinBox;

        private QLabel delaySpinBoxLabel;

        private QCheckBox hideThisWindowCheckBox;

        private QPushButton newScreenshotButton;

        private QPushButton saveScreenshotButton;

        private QPushButton quitScreenshotButton;

        private QVBoxLayout mainLayout;

        private QGridLayout optionsGroupBoxLayout;

        private QHBoxLayout buttonsLayout;

        #endregion

        /// <summary>
        /// Конструктор.
        /// </summary>
        public ScreenShot()
        {
            this.InitUi();

            this.WindowTitle = "Screenshot";
            this.Show();
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        public void InitUi()
        {
            this.screenshotLabel = new QLabel();
            this.screenshotLabel.SetSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding);
            this.screenshotLabel.Alignment = AlignmentFlag.AlignCenter;
            this.screenshotLabel.SetMinimumSize(240, 160);

            this.CreateOptionsGroupBox();
            this.CreateButtonsLayout();

            this.mainLayout = new QVBoxLayout();
            this.mainLayout.AddWidget(this.screenshotLabel);
            this.mainLayout.AddWidget(this.optionsGroupBox);
            this.mainLayout.AddLayout(this.buttonsLayout);
            this.Layout = this.mainLayout;
        }

        /// <summary>
        /// The create options group box.
        /// </summary>
        private void CreateOptionsGroupBox()
        {
            this.optionsGroupBox = new QGroupBox("Options");

            // Устанавливаю суффикс вводимого значения и предельное значение для вводимого значения.
            this.delaySpinBox = new QSpinBox { Suffix = Tr(" s"), Maximum = 60 };

            Connect(this.delaySpinBox, SIGNAL("valueChanged(int)"), this, SLOT(SlotUpdateCheckBox));

            this.delaySpinBoxLabel = new QLabel("Screenshot Delay:");

            this.hideThisWindowCheckBox = new QCheckBox("Спрятать это окно");

            this.optionsGroupBoxLayout = new QGridLayout();
            this.optionsGroupBoxLayout.AddWidget(this.delaySpinBoxLabel, 0, 0);
            this.optionsGroupBoxLayout.AddWidget(this.delaySpinBox, 0, 1);
            this.optionsGroupBoxLayout.AddWidget(this.hideThisWindowCheckBox, 1, 0, 1, 2);
            this.optionsGroupBox.Layout = this.optionsGroupBoxLayout;
        }

        /// <summary>
        /// The create buttons layout.
        /// </summary>
        private void CreateButtonsLayout()
        {
            // TODO: newScreenshotButton = CreateButton(Tr("New Screenshot"), this, SLOT(newScreenshot()));

            // TODO: saveScreenshotButton = createButton(tr("Save Screenshot"), this, SLOT(saveScreenshot()));

            // TODO: quitScreenshotButton = createButton(tr("Quit"), this, SLOT(close()));

            this.buttonsLayout = new QHBoxLayout();
            this.buttonsLayout.AddStretch(0);

            // TODO: buttonsLayout->addWidget(newScreenshotButton);
            // TODO: buttonsLayout->addWidget(saveScreenshotButton);
            // TODO: buttonsLayout->addWidget(quitScreenshotButton);
        }

        [Q_SLOT(SlotUpdateCheckBox)]
        private void UpdateCheckBox()
        {
            if (this.delaySpinBox.Value == 0)
            {
                this.hideThisWindowCheckBox.SetDisabled(true);
                this.hideThisWindowCheckBox.Checked = false;
            }
            else
            {
                this.hideThisWindowCheckBox.SetDisabled(false);
            }
        }

        private QPushButton CreateButton(string text, QWidget receiver, string member)
        {
            QPushButton button = new QPushButton(text);

            Connect(button, SIGNAL("clicked()"), receiver, member);

            return button;
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new ScreenShot();
            return QApplication.Exec();
        }
    }
}

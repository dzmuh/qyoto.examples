﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace collidingmice
{
    using QtCore;

    using QtGui;

    public class Program : Qt
    {
        /// <summary>
        /// TODO The mouse count.
        /// </summary>
        private static int mouseCount = 7;

        /// <summary>
        /// TODO The scene.
        /// </summary>
        private static QGraphicsScene scene;

        /// <summary>
        /// TODO The main.
        /// </summary>
        /// <param name="args">
        /// TODO The args.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        [STAThread]
        public static int Main(string[] args)
        {
            // Q_INIT_RESOURCE("mice");
            new QApplication(args);

            scene = new QGraphicsScene();
            scene.SetSceneRect(-300, -300, 600, 600);
            scene.itemIndexMethod = QGraphicsScene.ItemIndexMethod.NoIndex;

            for (int i = 0; i < MouseCount; ++i)
            {
                Mouse mouse = new Mouse();
                mouse.SetPos(Math.Sin((i * 6.28) / MouseCount) * 200,
                              Math.Cos((i * 6.28) / MouseCount) * 200);
                scene.AddItem(mouse);
            }

            return QApplication.Exec();
        }
    }
}

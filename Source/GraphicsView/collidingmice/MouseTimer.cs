﻿namespace collidingmice
{
    using QtCore;

    public class MouseTimer : QObject
    {
        private Mouse mouse;

        public MouseTimer(Mouse mouse)
            : base((QObject)null)
        {
            this.mouse = mouse;
            StartTimer(1000 / 33);
        }

        protected override void TimerEvent(QTimerEvent e)
        {
            this.mouse.TimerEvent(e);
        }
    }
}
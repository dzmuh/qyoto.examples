﻿namespace CalcExpression
{
    using System;
    using QtCore;
    using QtGui;

    public class QyotoProgram : QDialog
    {
        private QTextBrowser browser;

        private QLineEdit lineEdit;

        private QVBoxLayout layout;

        const string SlotTrim = "slot_Trim()";

        /// <summary>
        /// Конструктор.
        /// </summary>
        public QyotoProgram()
        {
            // Creating a text browser.
            this.browser = new QTextBrowser(this);
            
            // Creating a line edit for entering expression.
            this.lineEdit = new QLineEdit("Введите выражение и нажмите Enter: ", this);

            // Selecting the whole text by default.
            this.lineEdit.SelectAll();

            this.layout = new QVBoxLayout(this);
            this.layout.AddWidget(this.browser);
            this.layout.AddWidget(this.lineEdit);

            // Setting focus to lineedit.
            this.lineEdit.SetFocus();

            Connect(this.lineEdit, SIGNAL("returnPressed()"), this, SLOT(SlotTrim));

            this.WindowTitle = "Калькулятор";

            this.Show();
        }

        [Q_SLOT(SlotTrim)]
        private void updateUi()
        {
            // extracting the text in the lineedit
            var text = this.lineEdit.Text;

            try
            {
                // evaluating the expression and showing in browser
                this.browser.Append(string.Format("{0} = <b>{1}</b>", text, "TODO: результат")); // TODO

                // clearing the expression lineedit
                this.lineEdit.Clear();
            }
            catch
            {
                // if invalid expression, show msg
                this.browser.Append(
                    string.Format("<font color=red>Invalid Expression</font> - {0}", text));
            }
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

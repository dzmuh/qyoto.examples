﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Introduction to Qyoto: http://zetcode.com/gui/csharpqyoto/introduction
// 
//   This program centers a window on the screen.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;
    using QtCore;
    using QtGui;

    /// <summary>
    /// This program centers a window on the screen.
    /// </summary>
    public class QyotoProgram : QWidget
    {
        private const int WIDTH = 250;
        private const int HEIGHT = 150;

        /// <summary>
        /// Конструктор.
        /// </summary>
        public QyotoProgram()
        {
            this.WindowTitle = "Center";
            this.Resize(WIDTH, HEIGHT);
            this.Center();
            this.Show();
        }

        /// <summary>
        /// Метод выравнивания окна приложения по центру экрана.
        /// </summary>
        private void Center()
        {
            QDesktopWidget qdw = new QDesktopWidget();

            int screenWidth = qdw.Width;
            int screenHeight = qdw.Height;

            int cx = (screenWidth - WIDTH) / 2;
            int cy = (screenHeight - HEIGHT) / 2;

            this.Move(cx, cy);
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

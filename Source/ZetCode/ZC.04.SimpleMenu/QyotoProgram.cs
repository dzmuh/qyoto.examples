﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Menus and toolbars in Qyoto: http://zetcode.com/gui/csharpqyoto/menustoolbars/
// 
//   This program shows a simple menu. It has one action, which will terminate the program, when selected.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;
    using QtCore;
    using QtGui;

    /// <summary>
    /// This program shows a simple menu. It has one action, which will terminate the program, when selected.
    /// </summary>
    public class QyotoProgram : QMainWindow
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public QyotoProgram()
        {
            this.WindowTitle = "Simple menu";

            this.InitUi();

            this.Resize(300, 200);
            this.Move(300, 300);
            this.Show();
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        private void InitUi()
        {
            QAction quit = new QAction("&Quit", this);

            QMenu file = MenuBar.AddMenu("&File");
            file.AddAction(quit);

            Connect(quit, SIGNAL("triggered()"), qApp, SLOT("quit()"));
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

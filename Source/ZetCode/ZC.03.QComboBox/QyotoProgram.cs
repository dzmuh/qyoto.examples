﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Widgets in Qyoto: http://zetcode.com/gui/csharpqyoto/widgets/
// 
//   This program uses the QComboBox widget. The option selected from the combo box is displayed in the label widget.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;
    using QtCore;
    using QtGui;

    /// <summary>
    /// This program uses the QComboBox widget. The option selected from the combo box is displayed in the label widget.
    /// </summary>
    public class QyotoProgram : QWidget
    {
        private QLabel label;

        /// <summary>
        /// Конструктор.
        /// </summary>
        private QyotoProgram()
        {
            this.WindowTitle = "QComboBox";

            this.InitUi();

            this.Resize(250, 150);
            this.Move(300, 300);
            this.Show();
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        private void InitUi()
        {
            this.label = new QLabel("OpenSuse", this);

            QComboBox combo = new QComboBox(this);
            combo.AddItem("OpenSuse");
            combo.AddItem("Ubuntu");
            combo.AddItem("Arch");
            combo.AddItem("Fedora");
            combo.AddItem("Red Hat");
            combo.AddItem("Gentoo");

            combo.Move(50, 30);
            this.label.Move(50, 100);

            combo.ActivatedString += this.OnActivated;
        }

        [Q_SLOT]
        public void OnActivated(string text)
        {
            this.label.Text = text;
            this.label.AdjustSize();
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

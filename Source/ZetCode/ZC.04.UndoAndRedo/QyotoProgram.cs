﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Menus and toolbars in Qyoto: http://zetcode.com/gui/csharpqyoto/menustoolbars/
// 
//   This program disables/enables toolbuttons on a toolbar.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;

    using QtCore;
    using QtGui;

    /// <summary>
    /// This program disables/enables toolbuttons on a toolbar.
    /// </summary>
    public class QyotoProgram : QMainWindow
    {
        int count = 0;
        QToolButton undoButton;
        QToolButton redoButton;

        /// <summary>
        /// Конструктор.
        /// </summary>
        public QyotoProgram()
        {
            this.WindowTitle = "Undo redo";

            this.InitUi();

            this.Resize(300, 200);
            this.Move(300, 300);
            this.Show();
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        private void InitUi()
        {
            QIcon undoi  = new QIcon("../../images/undo.svg");
            QIcon redoi  = new QIcon("../../images/redo.svg");
            QIcon quitpi = new QIcon("../../images/quit.svg");

            QToolBar toolbar = new QToolBar();
            this.undoButton = new QToolButton();
            this.redoButton = new QToolButton();

            QAction undoAction = new QAction(undoi, "Undo", this.undoButton);
            QAction redoAction = new QAction(redoi, "Redo", this.redoButton);

            this.undoButton.DefaultAction = undoAction;
            this.redoButton.DefaultAction = redoAction;

            toolbar.AddWidget(this.undoButton);
            toolbar.AddWidget(this.redoButton);
            toolbar.AddSeparator();

            QAction quit = toolbar.AddAction(quitpi, "Quit Application");

            this.undoButton.Triggered += this.Count;
            this.redoButton.Triggered += this.Count;

            Connect(quit, SIGNAL("triggered()"), qApp, SLOT("quit()"));

            this.AddToolBar(toolbar);
        }

        [Q_SLOT]
        private void Count(QAction action)
        {
            if ("Undo".Equals(action.Text))
            {
                this.count += -1;
            }
            else
            {
                this.count += 1;
            }

            if (this.count <= 0)
            {
                this.undoButton.SetDisabled(true);
                this.redoButton.SetDisabled(false);
            }

            if (this.count >= 5)
            {
                this.undoButton.SetDisabled(false);
                this.redoButton.SetDisabled(true);
            }
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

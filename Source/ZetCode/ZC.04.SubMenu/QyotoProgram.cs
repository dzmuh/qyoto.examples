﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Menus and toolbars in Qyoto: http://zetcode.com/gui/csharpqyoto/menustoolbars/
// 
//   This program creates a submenu.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;
    using QtCore;
    using QtGui;

    /// <summary>
    /// This program creates a submenu.
    /// </summary>
    public class QyotoProgram : QMainWindow
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public QyotoProgram()
        {
            this.WindowTitle = "Submenu";

            this.InitUi();

            this.Resize(300, 200);
            this.Move(300, 300);
            this.Show();
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        private void InitUi()
        {
            QAction quit = new QAction("&Quit", this);

            QMenu file = MenuBar.AddMenu("&File");
            QMenu impm = new QMenu("Import");

            QAction seeds = new QAction("Import news feed...", this);
            QAction marks = new QAction("Import bookmarks...", this);
            QAction mail = new QAction("Import mail...", this);

            impm.AddAction(seeds);
            impm.AddAction(marks);
            impm.AddAction(mail);

            file.AddMenu(impm);
            file.AddAction(quit);

            Connect(quit, SIGNAL("triggered()"), qApp, SLOT("quit()"));
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

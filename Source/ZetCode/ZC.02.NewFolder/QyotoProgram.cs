﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Layout management в Qyoto: http://zetcode.com/gui/csharpqyoto/layoutmanagement/
// 
//   In this program, use the QGridLayout manager to create a New Folder example.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;
    using QtCore;
    using QtGui;

    /// <summary>
    /// In this program, use the QGridLayout manager to create a New Folder example.
    /// </summary>
    public class QyotoProgram : QWidget
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public QyotoProgram()
        {
            this.WindowTitle = "New Folder";

            this.InitUi();

            this.Resize(300, 300);
            this.Move(300, 300);
            this.Show();
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        private void InitUi()
        {
            QGridLayout grid = new QGridLayout(this);

            QLabel nameLabel = new QLabel("Name", this);
            QLineEdit nameEdit = new QLineEdit(this);
            QTextEdit text = new QTextEdit(this);
            QPushButton okButton = new QPushButton("OK", this);
            QPushButton closeButton = new QPushButton("Close", this);

            grid.AddWidget(nameLabel, 0, 0);
            grid.AddWidget(nameEdit, 0, 1, 1, 3);
            grid.AddWidget(text, 1, 0, 2, 4);
            grid.SetColumnStretch(1, 1);
            grid.AddWidget(okButton, 4, 2);
            grid.AddWidget(closeButton, 4, 3);
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

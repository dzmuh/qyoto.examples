﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Widgets in Qyoto: http://zetcode.com/gui/csharpqyoto/widgets/
// 
//   This program uses toggle buttons to change the background colour of a widget.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;
    using QtCore;
    using QtGui;

    /// <summary>
    /// This program uses toggle buttons to change the background colour of a widget.
    /// </summary>
    public class QyotoProgram : QWidget
    {
        /// <summary>
        /// Прямоугольник, который будем использовать для отрисовки цвета.
        /// </summary>
        private QWidget square;

        /// <summary>
        /// Цвет
        /// </summary>
        private QColor col;

        /// <summary>
        /// Красная кнопочка.
        /// </summary>
        private QPushButton redButton;

        /// <summary>
        /// Зелёная кнопочка.
        /// </summary>
        private QPushButton greenButton;

        /// <summary>
        /// Голубая кнопочка.
        /// </summary>
        private QPushButton blueButton;

        /// <summary>
        /// Конструктор.
        /// </summary>
        public QyotoProgram()
        {
            this.WindowTitle = "Toggle buttons";

            this.InitUi();

            this.Resize(350, 240);
            this.Move(400, 300);
            this.Show();
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        private void InitUi()
        {
            this.col = new QColor();

            this.redButton = new QPushButton("Red", this) { Checkable = true };
            this.greenButton = new QPushButton("Green", this) { Checkable = true };
            this.blueButton = new QPushButton("Blue", this) { Checkable = true };

            this.redButton.Toggled += this.OnToggled;
            this.greenButton.Toggled += this.OnToggled;
            this.blueButton.Toggled += this.OnToggled;

            this.square = new QWidget(this) { StyleSheet = "QWidget { background-color: black }" };

            this.redButton.Move(30, 30);
            this.greenButton.Move(30, 80);
            this.blueButton.Move(30, 130);
            this.square.SetGeometry(150, 25, 150, 150);
        }

        [Q_SLOT]
        public void OnToggled(bool @checked)
        {
            int red = this.redButton.Checked ? 255 : 0;
            int green = this.greenButton.Checked ? 255 : 0;
            int blue = this.blueButton.Checked ? 255 : 0;

            this.col = new QColor(red, green, blue);

            string sheet = string.Format("QWidget {{ background-color: {0} }}", this.col.Name);
            this.square.StyleSheet = sheet;
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

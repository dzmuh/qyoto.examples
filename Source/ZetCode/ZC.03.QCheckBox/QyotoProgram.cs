﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Widgets in Qyoto: http://zetcode.com/gui/csharpqyoto/widgets/
// 
//   Эта программа использует виджет QCheckBox для показа/скрытия заголовка своего окна.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;
    using QtCore;
    using QtGui;

    /// <summary>
    /// Эта программа использует виджет QCheckBox для показа/скрытия заголовка своего окна.
    /// </summary>
    public class QyotoProgram : QWidget
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public QyotoProgram()
        {
            this.WindowTitle = "QCheckBox";

            this.SetupUi();

            this.Resize(250, 150);
            this.Move(300, 300);
            this.Show();
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        public void SetupUi()
        {
            QCheckBox cb = new QCheckBox("Show Title", this);
            cb.Checked = true;
            cb.Move(50, 50);

            cb.StateChanged += this.ShowTitle;
        }

        [Q_SLOT]
        public void ShowTitle(int state)
        {
            // TODO: Mono?
            //this.WindowTitle = state == (int)CheckState.Checked ? "QCheckBox" : "";
            if (state == (int)CheckState.Checked)
            {
                this.WindowTitle = "QCheckBox";
            }
            else
            {
                this.WindowTitle = "";
            }
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

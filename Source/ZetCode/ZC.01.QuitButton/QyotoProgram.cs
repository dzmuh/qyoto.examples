﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Introduction to Qyoto: http://zetcode.com/gui/csharpqyoto/introduction
// 
//   This program creates a quit button. When we press the button, the application terminates.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;
    using QtCore;
    using QtGui;

    /// <summary>
    /// This program creates a quit button.
    /// When we press the button, the application terminates.
    /// </summary>
    public class QyotoProgram : QWidget
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public QyotoProgram()
        {
            this.WindowTitle = "Quit button";

            this.InitUI();

            this.Resize(250, 150);
            this.Move(300, 300);
            this.Show();
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        public void InitUI()
        {
            QPushButton quit = new QPushButton("Quit", this);

            Connect(quit, SIGNAL("clicked()"), qApp, SLOT("quit()"));
            quit.SetGeometry(50, 40, 80, 30);
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

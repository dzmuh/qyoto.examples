﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Introduction to Qyoto: http://zetcode.com/gui/csharpqyoto/introduction
//   This program displays a tooltip.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;

    using QtCore;

    using QtGui;

    /// <summary>
    /// This program displays a tooltip.
    /// </summary>
    public class QyotoProgram : QWidget
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        private QyotoProgram() 
        {
            this.WindowTitle = "Tooltip";

            this.ToolTip = "This is QWidget";
            this.Resize(250, 150);
            this.Move(300, 300);
            this.Show();
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Menus and toolbars in Qyoto: http://zetcode.com/gui/csharpqyoto/menustoolbars/
// 
//   This program creates a toolbar.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;
    using QtCore;
    using QtGui;

    /// <summary>
    /// This program creates a toolbar.
    /// </summary>
    public class QyotoProgram : QMainWindow
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public QyotoProgram()
        {
            this.WindowTitle = "Toolbar";

            this.InitUi();

            this.Resize(300, 200);
            this.Move(300, 300);
            this.Show();
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        private void InitUi()
        {
            QIcon newpi = new QIcon("../../images/new.png");
            QIcon openpi = new QIcon("../../images/open.svg");
            QIcon quitpi = new QIcon("../../images/quit.png");

            QToolBar toolbar = AddToolBar("main toolbar");
            toolbar.AddAction(newpi, "New File");
            toolbar.AddAction(openpi, "Open File");
            toolbar.AddSeparator();
            QAction quit = toolbar.AddAction(quitpi, "Quit Application");

            Connect(quit, SIGNAL("triggered()"), qApp, SLOT("quit()"));
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Layout management в Qyoto: http://zetcode.com/gui/csharpqyoto/layoutmanagement/
// 
//   In this program, use box layouts to position two buttons in the bottom right corner of the window.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;
    using QtCore;
    using QtGui;

    /// <summary>
    /// In this program, use box layouts to position two buttons in the bottom right corner of the window.
    /// </summary>
    public class QyotoProgram : QWidget
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        private QyotoProgram()
        {
            this.WindowTitle = "Buttons";

            this.InitUi();

            this.Resize(300, 150);
            this.Move(300, 300);
            this.Show();
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        private void InitUi()
        {
            QVBoxLayout vbox = new QVBoxLayout(this);
            QHBoxLayout hbox = new QHBoxLayout();

            QPushButton ok = new QPushButton("OK", this);
            QPushButton apply = new QPushButton("Apply", this);

            hbox.AddWidget(ok, 1, AlignmentFlag.AlignRight);
            hbox.AddWidget(apply);

            vbox.AddStretch(1);
            vbox.AddLayout(hbox);
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

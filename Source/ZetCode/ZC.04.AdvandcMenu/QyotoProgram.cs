﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Menus and toolbars in Qyoto: http://zetcode.com/gui/csharpqyoto/menustoolbars/
// 
//   This program shows image menu items, a shorcut and a separator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;
    using QtCore;
    using QtGui;

    /// <summary>
    /// This program shows image menu items, a shorcut and a separator.
    /// </summary>
    public class QyotoProgram : QMainWindow
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public QyotoProgram()
        {
            this.WindowTitle = "Image menu";

            this.InitUi();

            this.Resize(300, 200);
            this.Move(300, 300);
            this.Show();
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        private void InitUi()
        {
            QIcon newpix  = new QIcon("../../images/new.png");
            QIcon openpix = new QIcon("../../images/open.svg");
            QIcon quitpix = new QIcon("../../images/quit.png");

            QAction newa = new QAction(newpix, "&New", this);
            QAction open = new QAction(openpix, "&Open", this);
            QAction quit = new QAction(quitpix, "&Quit", this);
            quit.Shortcut = "CTRL+Q";

            QMenu file;
            file = MenuBar.AddMenu("&File");
            file.AddAction(newa);
            file.AddAction(open);
            file.AddSeparator();
            file.AddAction(quit);

            Connect(quit, SIGNAL("triggered()"), qApp, SLOT("quit()"));
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Layout management в Qyoto: http://zetcode.com/gui/csharpqyoto/layoutmanagement/
// 
//   In this program, use box layouts to create a windows example.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;
    using QtCore;
    using QtGui;

    /// <summary>
    /// In this program, use box layouts to create a windows example.
    /// </summary>
    public class QyotoProgram : QWidget
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public QyotoProgram()
        {
            this.WindowTitle = "Windows";

            this.InitUi();

            this.Resize(350, 300);
            this.Move(300, 300);
            this.Show();
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        private void InitUi()
        {
            QVBoxLayout vbox = new QVBoxLayout(this);

            QVBoxLayout vbox1 = new QVBoxLayout();
            QHBoxLayout hbox1 = new QHBoxLayout();
            QHBoxLayout hbox2 = new QHBoxLayout();

            QLabel windLabel = new QLabel("Windows", this);
            QTextEdit edit = new QTextEdit(this);
            edit.Enabled = false;

            QPushButton activate = new QPushButton("Activate", this);
            QPushButton close = new QPushButton("Close", this);
            QPushButton help = new QPushButton("Help", this);
            QPushButton ok = new QPushButton("OK", this);

            vbox.AddWidget(windLabel);

            vbox1.AddWidget(activate);
            vbox1.AddWidget(close, 0, AlignmentFlag.AlignTop);
            hbox1.AddWidget(edit);
            hbox1.AddLayout(vbox1);

            vbox.AddLayout(hbox1);

            hbox2.AddWidget(help);
            hbox2.AddStretch(1);
            hbox2.AddWidget(ok);

            vbox.AddLayout(hbox2, 1);

            this.Layout = vbox;
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

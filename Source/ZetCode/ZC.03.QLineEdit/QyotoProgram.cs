﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Widgets in Qyoto: http://zetcode.com/gui/csharpqyoto/widgets/
// 
//   This program shows text which is entered in a QLineEdit widget in a QLabel widget.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;
    using QtCore;
    using QtGui;

    /// <summary>
    /// This program shows text which is entered in a QLineEdit widget in a QLabel widget.
    /// </summary>
    public class QyotoProgram : QWidget
    {
        QLabel label;

        /// <summary>
        /// Конструктор.
        /// </summary>
        public QyotoProgram()
        {
            this.WindowTitle = "QLineEdit";

            this.InitUi();

            this.Resize(250, 150);
            this.Move(400, 300);
            this.Show();
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        public void InitUi()
        {
            this.label = new QLabel(this);

            QLineEdit edit = new QLineEdit(this);
            edit.TextChanged += this.OnChanged;

            edit.Move(60, 100);
            this.label.Move(60, 40);
        }

        [Q_SLOT]
        public void OnChanged(string text)
        {
            this.label.Text = text;
            this.label.AdjustSize();
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

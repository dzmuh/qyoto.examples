﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ZetCode" file="QyotoProgram.cs">
//   ZetCode Qyoto C# tutorial
// </copyright>
// <author>Jan Bodnar</author>
// <summary>
//   По мотивам статьи Layout management в Qyoto: http://zetcode.com/gui/csharpqyoto/layoutmanagement/
// 
//   In this program, we lay out widgets using absolute positioning
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;
    using QtCore;
    using QtGui;

    /// <summary>
    /// In this program, we lay out widgets using absolute positioning
    /// </summary>
    public class QyotoProgram : QWidget
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public QyotoProgram()
        {
            this.WindowTitle = "Absolute";

            this.InitUi();

            this.Resize(300, 280);
            this.Move(300, 300);
            this.Show();
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        private void InitUi()
        {
            this.StyleSheet = "QWidget { background-color: #414141 }";

            QPixmap bardejov = new QPixmap("../../images/appbar.acorn.jpg");
            QPixmap rotunda = new QPixmap("../../images/appbar.cards.heart.png");
            QPixmap mincol = new QPixmap("../../images/appbar.chat.png");

            QLabel barLabel = new QLabel(this);
            barLabel.Pixmap = bardejov;
            barLabel.Move(20, 20);

            QLabel rotLabel = new QLabel(this);
            rotLabel.Pixmap = rotunda;
            rotLabel.Move(40, 160);

            QLabel minLabel = new QLabel(this);
            minLabel.Pixmap = mincol;
            minLabel.Move(170, 50);
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

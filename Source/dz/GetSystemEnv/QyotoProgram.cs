﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QyotoProgram.cs" company="Dzmuh and Co">
//   License: Public Domain, https://creativecommons.org/licenses/publicdomain/
// </copyright>
// <author>Frolkov Iliya A. (aka Dzmuh)</author>
// <summary>
//   Программа которая выводит окно с данными по платформе, версию операционной системы,
//   данными по установленному сервис паку (актуально для Windows) используя статичный класс Environment
//   из пространства имён System.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace QyotoExamples
{
    using System;
    using System.Globalization;

    using QtCore;
    using QtGui;

    /// <summary>
    /// Программа которая выводит окно с данными по платформе, версию операционной системы,
    /// данными по установленному сервис паку (актуально для Windows) используя статичный класс
    /// <see cref="System.Environment"/> из пространства имён <see cref="System"/>.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Класс <see cref="System.Environment"/> содержит свойства возвращающие: идентификатор платформы,
    /// строку описывающую сервис пак, детализированное описание версии ОС.
    /// </para>
    /// <para>
    /// В качестве платформы может быть: Win32s (расширение 16-битной Windows до 32-х бит),
    /// Windows95/Windows98, Windows NT, Windows CE, Unix, Xbox 360, MacOSX.
    /// </para>
    /// </remarks>
    public class QyotoProgram : QWidget
    {
        private const int WIDTH = 800;
        private const int HEIGHT = 600;

        /// <summary>
        /// Конструктор.
        /// </summary>
        public QyotoProgram()
        {
            this.WindowTitle = "System Environment";

            this.InitUi();

            this.Resize(WIDTH, HEIGHT);
            this.CenterWindow();
            this.Show();
        }

        /// <summary>
        /// Метод выравнивания окна приложения по центру экрана.
        /// </summary>
        private void CenterWindow()
        {
            QDesktopWidget qdw = new QDesktopWidget();

            int screenWidth = qdw.Width;
            int screenHeight = qdw.Height;

            int cx = (screenWidth - WIDTH) / 2;
            int cy = (screenHeight - HEIGHT) / 2;

            this.Move(cx, cy);
        }

        /// <summary>
        /// Инициализация интерфейса пользователя.
        /// </summary>
        private void InitUi()
        {
            OperatingSystem currOs = Environment.OSVersion;

            QGridLayout grid = new QGridLayout(this);

            QLabel labelOsPlatformTitle = new QLabel("OperatingSytem.Platform", this);
            QLabel labelOsPlatformValue = new QLabel(currOs.Platform.ToString(), this);
            grid.AddWidget(labelOsPlatformTitle, 0, 0);
            grid.AddWidget(labelOsPlatformValue, 0, 1);

            QLabel labelOsServicePackTitle = new QLabel("OperatingSytem.ServicePack", this);
            QLabel labelOsServicePackValue = new QLabel(currOs.ServicePack.ToString(CultureInfo.InvariantCulture), this);
            grid.AddWidget(labelOsServicePackTitle, 1, 0);
            grid.AddWidget(labelOsServicePackValue, 1, 1);

            QLabel labelOsVersionTitle = new QLabel("OperatingSytem.Version", this);
            QLabel labelOsVersionValue = new QLabel(currOs.Version.ToString(), this);
            grid.AddWidget(labelOsVersionTitle, 2, 0);
            grid.AddWidget(labelOsVersionValue, 2, 1);

            QLabel labelOsTotalTitle = new QLabel("OS Version (OperatingSytem) total", this);
            QLabel labelOsTotalValue = new QLabel(currOs.ToString(), this);
            grid.AddWidget(labelOsTotalTitle, 3, 0);
            grid.AddWidget(labelOsTotalValue, 3, 1);

            QLabel labelClrVersionTitle = new QLabel("Версия CLR", this);
            QLabel labelClrVersionValue = new QLabel(Environment.Version.ToString(), this);
            grid.AddWidget(labelClrVersionTitle, 4, 0);
            grid.AddWidget(labelClrVersionValue, 4, 1);
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new QyotoProgram();
            return QApplication.Exec();
        }
    }
}

﻿namespace HappyDay
{
    using System;

    using QtCore;
    using QtGui;

    class Program : QWidget
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public Program()
        {
            this.WindowTitle = "С Днём Рождения тя няша!!!";

            this.Show();
        }

        [STAThread]
        public static int Main(string[] args)
        {
            new QApplication(args);
            new Program();
            return QApplication.Exec();
        }
    }
}
